#!/bin/bash

# Check if the .pidfile exists
if [ -f .pidfile ]; then
    # Read the PID from the .pidfile
    PID=$(cat .pidfile)

    # Check if the process with the retrieved PID is running
    if ps -p $PID > /dev/null; then
        # If it's running, stop the process gracefully
        kill $PID
        echo "Stopped the process with PID $PID"
    else
        echo "No process with PID $PID is running"
    fi

    # Remove the .pidfile
    rm .pidfile
else
    echo ".pidfile not found. The process may not be running."
fi
