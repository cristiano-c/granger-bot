import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import { logger } from '../helpers'
import { LogsCollection } from '../database/LogsCollection'

export async function showLogs (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    let n = parseInt(ctx.message?.text?.split(' ').slice(1).join(' ') ?? '10')
    if (Number.isNaN(n)) { n = 10 }
    const logDocuments = await LogsCollection.getInstance().getLastNDocuments(n)
    const logs = logDocuments.map((log) => {
      return `[${log.tracked.toLocaleString()}] (${log.username})- ${log.action} - ${log.message ?? ''}\n`
    })
    await ctx.reply(`Ecco l'elenco degli ultimi ${n} log:\n\n${logs.join('\n')}`)
    logger(ctx.from?.username ?? 'unknown', `Ha richiesto di visualizzare gli ultimi ${n} log.`)
  } catch (e) {
    console.error(e)
  }
}
