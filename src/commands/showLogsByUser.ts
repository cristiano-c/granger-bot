import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import { logger } from '../helpers'
import { LogsCollection } from '../database/LogsCollection'

export async function showLogsByUser (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    const user = ctx.message?.text?.split(' ')[1] ?? 'unknown'
    const n = parseInt(ctx.message?.text?.split(' ')[2] ?? '10')
    const logDocuments = await LogsCollection.getInstance().getLastNDocumentsByUser(user, n)
    const logs = logDocuments.map((log) => {
      return `[${log.tracked.toLocaleString()}] - ${log.action} - ${log.message ?? ''}\n`
    })
    await ctx.reply(`Ecco l'elenco degli ultimi ${n} log di ${user}:\n\n${logs.join('\n')}`)
    logger(ctx.from?.username ?? 'unknown', `Ha richiesto di visualizzare gli ultimi ${n} log.`)
  } catch (e) {
    console.error(e)
  }
}
