import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import PermissionsCollection from '../database/PermissionsCollection'
import { logger } from '../helpers'

export async function removePermissionCommand (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    const username: string | undefined = ctx.message?.text?.split(' ')[1]
    if (username === undefined || username === null || username === '') {
      throw new Error('username is null')
    }
    const permissionsCollection = new PermissionsCollection()
    await permissionsCollection.removeUserPermission(username)
    await ctx.reply('È stato rimosso il permesso di utilizzare il bot a ' + username + '.')
    logger(username, 'Rimosso il permesso a ' + username + '.')
  } catch (e) {
    console.error(e)
  }
}
