import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import { logger } from '../helpers'

export async function showHelp (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    const helpList = [
      ' - /logs [n] per visualizzare gli ultimi n log di tutti',
      ' - /logsbyuser [username] [n] per visualizzare gli ultimi n log di un utente',
      ' - /resetcomestai per resettare i messaggi di "Come stai?" usa il comando ',
      ' - /resetcomestai per resettare i messaggi di "Come stai?" usa il comando ',
      ' - /addcomestai per aggiungere un messaggio di "Come stai?" usa il comando ',
      ' - /addPermission per aggiungere un permesso di utilizzo del bot usa il comando ',
      ' - /removePermission per rimuovere un permesso di utilizzo del bot usa il comando ',
      ' - Invia una foto con una didascalia per aggiungere una nuova foto del giorno'
    ]
    await ctx.reply('Ecco la lista dei comandi admin:\n' + helpList.join('\n'))
    logger(ctx.from?.username ?? 'unknown', 'Resettati i messaggi di "Come stai?".')
  } catch (e) {
    console.error(e)
  }
}
