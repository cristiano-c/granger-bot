import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import { logger } from '../helpers'
import { ComeStaiCollection } from '../database/ComeStaiCollection'

export async function addComeStaiMessage (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    const message: string | undefined = ctx.message?.text?.split(' ').slice(1).join(' ')
    if (message === undefined || message === null || message === '') {
      throw new Error('message cannot be empty')
    }
    await ComeStaiCollection.getInstance().insertOne(message)
    await ctx.reply('È stato aggiunto il messaggio "' + message + '".')
    logger(ctx.from?.username ?? 'unknown', 'Aggiunto nuovo messaggio.', message)
  } catch (e) {
    console.error(e)
  }
}
