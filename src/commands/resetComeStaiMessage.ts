import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import { logger } from '../helpers'
import { ComeStaiCollection } from '../database/ComeStaiCollection'

export async function resetComeStaiMessage (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    await ComeStaiCollection.getInstance().deleteMany()
    await ctx.reply('I messaggi del button "Come stai?" sono stati resettati.')
    logger(ctx.from?.username ?? 'unknown', 'Resettati i messaggi di "Come stai?".')
  } catch (e) {
    console.error(e)
  }
}
