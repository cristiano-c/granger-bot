import { type Context } from 'grammy'
import { onlyParents } from '../configurations/middlewares'
import PermissionsCollection from '../database/PermissionsCollection'
import { logger } from '../helpers'

export async function addPermissionCommand (ctx: Context): Promise<void> {
  try {
    onlyParents(ctx)
    const username: string | undefined = ctx.message?.text?.split(' ')[1]
    if (username === undefined || username === null || username === '') {
      throw new Error('username cannot be empty')
    }
    const permissionsCollection = new PermissionsCollection()
    await permissionsCollection.addUserPermission(username)
    await ctx.reply('È stato aggiunto il permesso di utilizzare il bot a ' + username + '.')
    logger(username, 'Aggiunto permesso a ' + username + '.')
  } catch (e) {
    console.error(e)
  }
}
