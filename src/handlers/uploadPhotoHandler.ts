import { type Bot, type Context } from 'grammy'
import path from 'path'
import axios from 'axios'
import ImagesCollection from '../database/ImagesCollection'
import { logger } from '../helpers'
import { menu, promtpMessage } from '../menu/mainMenu'

export function defaultReply (ctx: Context): void {
  const message: string | undefined = ctx.message?.text
  logger(ctx.from?.username ?? 'unknown', 'Ha mandato un messaggio di testo', ctx.message?.text)
  if (message === '/start' || message === '/menu ' || message === 'menu' || message === 'start ') {
    void ctx.reply(promtpMessage, { reply_markup: menu })
  } else {
    void ctx.reply('Purtroppo non ho ancora la possibilità di rispondere ai messaggi di testo. Per favore utilizza il comando /start per vedere cosa posso fare.\n\nUnfortunately I cannot reply to text messages yet. Please use the /start command to see what I can do.\n\n残念ながら、まだテキストメッセージに返信することはできません。 私ができることを確認するには、/start コマンドを使用してください。')
  }
}

export function uploadPhotoHandler (ctx: Context, bot: Bot): void {
  if (ctx.message === undefined) {
    console.log('ctx.message is undefined')
    return
  }
  const message = ctx.message
  if (message.caption === undefined) {
    void ctx.reply('Ho bisogno di una descrizione per salvare la foto. Per favore, rimandami la foto con una descrizione.')
    return
  }
  const caption = ctx.message.caption

  if (message.photo === undefined) {
    void ctx.reply('Ho bisogno di una foto per salvare la foto. Per favore, rimandami la foto con una foto. :D')
    return
  }
  const photo = message.photo[message.photo.length - 1]
  if (photo === undefined) {
    return
  }
  const fileId = photo.file_id
  ctx.api.getFile(fileId).then(async (fileInfo: File) => {
    fileInfo = JSON.parse(JSON.stringify(fileInfo))
    console.log(fileInfo)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    const fileUrl = `https://api.telegram.org/file/bot${bot.token}/${fileInfo.file_path}`
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/ban-ts-comment
    // @ts-expect-error
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const filePath = path.join(__dirname, fileInfo.file_path.split('/').pop())

    const response = await axios({
      method: 'get',
      url: fileUrl,
      responseType: 'arraybuffer'
    })

    const imagesCollection = new ImagesCollection()
    imagesCollection.saveImage(response.data, filePath, caption).then(() => {
      void ctx.reply('Ho salvato la nuova foto, grazie!')
    }).catch((err) => {
      console.error(err)
      void ctx.reply('Non sono riuscito a salvare la foto: ' + err)
    })
  }).catch((err) => {
    console.error(err)
    void ctx.reply('error')
  })
}
