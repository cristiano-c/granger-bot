import { type Bot, type Context } from 'grammy'
import path from 'path'
import axios from 'axios'
import ImagesCollection from '../database/ImagesCollection'

export function uploadDocumentHandler (ctx: Context, bot: Bot): void {
  if (ctx.message === undefined) {
    return
  }
  if (ctx.message.document === undefined) {
    return
  }
  const document = ctx.message.document
  if (document === undefined) {
    return
  }
  if (ctx.message.caption === undefined) {
    void ctx.reply('Ho bisogno di una descrizione per salvare la foto. Per favore, rimandami la foto con una descrizione.')
    return
  }
  const caption = ctx.message.caption
  const fileId = document.file_id
  ctx.api.getFile(fileId).then(async (fileInfo: File) => {
    fileInfo = JSON.parse(JSON.stringify(fileInfo))
    console.log(fileInfo)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    const fileUrl = `https://api.telegram.org/file/bot${bot.token}/${fileInfo.file_path}`
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/ban-ts-comment
    // @ts-expect-error
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const filePath = path.join(__dirname, fileInfo.file_path.split('/').pop())

    const response = await axios({
      method: 'get',
      url: fileUrl,
      responseType: 'arraybuffer'
    })

    const imagesCollection = new ImagesCollection()
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    imagesCollection.saveImage(response.data, filePath, caption).then(() => {
      void ctx.reply('Ho salvato la nuova foto, grazie!')
    }).catch((err) => {
      console.error(err)
      void ctx.reply('Non sono riuscito a salvare la foto: ' + err)
    })
  }).catch((err) => {
    console.error(err)
    void ctx.reply('error')
  })
}
