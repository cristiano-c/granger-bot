import * as fs from 'fs'
import { type Log, LogsCollection } from './database/LogsCollection'

export function getToken (environment: string): string {
  let botTokenEnvName: string
  if (environment === Environment.PRODUCTION) {
    botTokenEnvName = 'ENCHANTING_HERMIONE_BOT'
  } else {
    botTokenEnvName = 'ENCHANTING_HERMIONE_TEST_BOT'
  }
  const botToken: string | undefined = process.env[botTokenEnvName]

  if (botToken == null) {
    console.error('ENCHANTING_HERMIONE_BOT is not defined in the environment.')
    process.exit(1)
  } else {
    console.log('ENCHANTING_HERMIONE_BOT is defined in the environment.')
  }
  return botToken
}

export function randomMessage (jsonFilePath: string): string {
  try {
    const jsonData = JSON.parse(fs.readFileSync(jsonFilePath, 'utf8'))

    if (Array.isArray(jsonData)) {
      const randomIndex = Math.floor(Math.random() * jsonData.length)
      return jsonData[randomIndex]
    } else {
      throw new Error('Il file JSON non contiene un array valido.')
    }
  } catch (error) {
    console.error(`Errore nella lettura del file JSON: ${JSON.stringify(error)}`)
    return '' // Puoi gestire l'errore in base alle tue esigenze
  }
}

export function humanDate (datetime: Date, locale: string = 'en-US'): string {
  const options: Intl.DateTimeFormatOptions = {
    weekday: 'long', // Full weekday name (e.g., "Monday")
    year: 'numeric', // Full numeric year (e.g., "2024")
    month: 'long', // Full month name (e.g., "January")
    day: 'numeric', // Day of the month (e.g., "10")
    hour: 'numeric', // Hour in 24-hour format (e.g., "17")
    minute: 'numeric', // Minutes (e.g., "29")
    second: 'numeric' // Seconds (e.g., "54")
  }

  return datetime.toLocaleString(locale, options)
}

export function logger (username: string, action: string, message?: string): void {
  const log: Log = {
    username,
    tracked: new Date(),
    message,
    action
  }
  void LogsCollection.getInstance().insertOne(log)
}

export function getEnvironment (): string {
  if (process.env.HERMIONE_ENVIRONMENT === undefined) {
    return Environment.PRODUCTION
  } else {
    return Environment.DEVELOPMENT
  }
}

export enum Environment {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development'
}
