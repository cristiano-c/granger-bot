import { Menu } from '@grammyjs/menu'
import { humanDate, logger, randomMessage } from '../helpers'
import ImagesCollection, { type ImageDocument } from '../database/ImagesCollection'
import { type Context, InputFile } from 'grammy'
import { ComeStaiCollection } from '../database/ComeStaiCollection'

export const promtpMessage = '🥰 Chattiamo ancora? Fammi una domanda tra le seguenti:'
const waitMultiplier = 35

function getUsername (ctx: Context): string {
  return ctx.from?.username ?? 'unknown'
}
async function delayAction (milliseconds: number): Promise<void> {
  await new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve() // Completa la promessa dopo il timeout
    }, milliseconds + 2000)
  })
}

async function replyHandlerWithRandomMessage (ctx: Context, repliesFilePath: string, buttonMessage: string): Promise<void> {
  const username: string = getUsername(ctx)
  logger(username, buttonMessage)
  const replyMessage: string = randomMessage(repliesFilePath)
  const waitFactor = replyMessage.length * waitMultiplier
  await ctx.reply(replyMessage)
  await delayAction(waitFactor)
  await ctx.reply(promtpMessage, { reply_markup: menu })
}

async function replyHandlerWithPhotoOfTheDay (ctx: Context, buttonMessage: string): Promise<void> {
  const username: string = getUsername(ctx)
  logger(username, buttonMessage)
  const imagesCollection = new ImagesCollection()
  const photoOfTheDay: ImageDocument = await imagesCollection.getRandomImageOfCurrentDay()
  // Sends the photo of the day to the user
  await ctx.replyWithPhoto(new InputFile(Buffer.from(photoOfTheDay.data, 'base64')), { caption: '"' + photoOfTheDay.description + '"\n\n\n' + humanDate(photoOfTheDay.uploaded, 'it-IT') })
  await delayAction(5000)
  await ctx.reply(promtpMessage, { reply_markup: menu })
}

async function replyHandlerWithRandomPhoto (ctx: Context, buttonMessage: string): Promise<void> {
  const username: string = getUsername(ctx)
  logger(username, buttonMessage)
  const imagesCollection = new ImagesCollection()
  const photoOfTheDay: ImageDocument = await imagesCollection.getRandomImage()
  // Sends the photo of the day to the user
  await ctx.replyWithPhoto(new InputFile(Buffer.from(photoOfTheDay.data, 'base64')), { caption: '"' + photoOfTheDay.description + '"\n\n\n' + humanDate(photoOfTheDay.uploaded, 'it-IT') })
  await delayAction(5000)
  await ctx.reply(promtpMessage, { reply_markup: menu })
}

async function replyWithMessage (ctx: Context, buttonMessage: string, message: string): Promise<void> {
  const username: string = getUsername(ctx)
  logger(username, buttonMessage)
  const replyMessage: string = message
  const waitFactor = replyMessage.length * waitMultiplier
  await ctx.reply(replyMessage)
  await delayAction(waitFactor)
  await ctx.reply(promtpMessage, { reply_markup: menu })
}

const buttonMessages = {
  stare_bene: 'Come stai oggi?',
  foto_oggi: 'Mi mandi una foto di oggi?',
  foto_casuale: 'Mi mandi una foto a caso?',
  nome: 'Cosa significa Hermione?',
  data_nascita: 'Quando sei nata?',
  luogo_nascita: 'Dove sei nata?',
  peso: 'Quanto pesavi alla nascita?',
  visita: 'Possiamo venire a trovarti?'
}

export const menu = new Menu('my-menu-identifier')
  .text(buttonMessages.stare_bene, async (ctx) => {
    const randomMessage = await ComeStaiCollection.getInstance().getRandomMessage()
    void replyWithMessage(ctx, buttonMessages.stare_bene, randomMessage)
  }).row()

  .text(buttonMessages.foto_oggi, async (ctx) => {
    await replyHandlerWithPhotoOfTheDay(ctx, buttonMessages.foto_oggi)
  }).row()

  .text(buttonMessages.foto_casuale, async (ctx) => {
    await replyHandlerWithRandomPhoto(ctx, buttonMessages.foto_casuale)
  }).row()

  .text(buttonMessages.nome, async (ctx) => {
    await replyHandlerWithRandomMessage(ctx, 'src/resources/nome.json', buttonMessages.nome)
  }).row()

  .text(buttonMessages.data_nascita, async (ctx) => {
    await replyHandlerWithRandomMessage(ctx, 'src/resources/data_nascita.json', buttonMessages.data_nascita)
  }).row()

  .text(buttonMessages.luogo_nascita, async (ctx) => {
    await replyHandlerWithRandomMessage(ctx, 'src/resources/luogo_nascita.json', buttonMessages.luogo_nascita)
  }).row()

  .text(buttonMessages.peso, async (ctx) => {
    await replyHandlerWithRandomMessage(ctx, 'src/resources/peso.json', buttonMessages.peso)
  }).row()

  .text(buttonMessages.visita, async (ctx) => {
    await replyHandlerWithRandomMessage(ctx, 'src/resources/visita.json', buttonMessages.visita)
  }).row()
