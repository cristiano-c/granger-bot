import { type Bot } from 'grammy'
import { menu } from '../menu/mainMenu'
import { randomMessage } from '../helpers'

export function addMenus (bot: Bot): void {
  bot.use(menu)
  bot.command('menu', async (ctx) => {
    await ctx.reply(randomMessage('src/resources/intro.json'), { reply_markup: menu })
  })
  bot.command('start', async (ctx) => {
    await ctx.reply(randomMessage('src/resources/intro.json'), { reply_markup: menu })
  })
}
