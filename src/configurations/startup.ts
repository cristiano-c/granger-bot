import { Bot } from 'grammy'
import { Environment, getEnvironment, getToken } from '../helpers'
import MongoDriver from '../database/MongoDriver'

export async function startupConfiguration (): Promise<Bot> {
  let dbUrl: string
  let dbName: string

  const environment: string = getEnvironment()

  if (environment === Environment.PRODUCTION) {
    console.log('Running in production mode')
    dbUrl = 'mongodb://192.168.1.104:27017'
    dbName = 'enchanting_hermione_bot'
  } else {
    console.log('Running in development mode')
    dbUrl = 'mongodb://127.0.0.1:27017'
    dbName = 'enchanting_hermione_test_bot'
  }

  const botToken = getToken(environment)
  const bot = new Bot(botToken)
  await MongoDriver.getInstance().initialize(dbUrl, dbName)

  return bot
}
