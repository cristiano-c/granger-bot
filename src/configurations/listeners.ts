import { type Bot } from 'grammy'
import { uploadDocumentHandler } from '../handlers/uploadDocumentHandler'
import { defaultReply, uploadPhotoHandler } from '../handlers/uploadPhotoHandler'
import { onlyParents } from './middlewares'

export function addListeners (bot: Bot): void {
  bot.on('message:photo', (ctx) => { onlyParents(ctx); uploadPhotoHandler(ctx, bot) })
  bot.on('message:document', (ctx) => { onlyParents(ctx); uploadDocumentHandler(ctx, bot) })
  bot.on('message', (ctx) => { defaultReply(ctx) })
}
