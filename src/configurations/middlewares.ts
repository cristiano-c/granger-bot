import { type Bot, type Context, type NextFunction } from 'grammy'
import PermissionsCollection from '../database/PermissionsCollection'

export function addMiddlewares (bot: Bot): void {
  bot.use(onlyAllowedUsernames)
}

export async function onlyAllowedUsernames (ctx: Context, next: NextFunction): Promise<void> {
  const username = ctx.from?.username
  if (username == null) {
    throw new Error('username is null')
  }
  const permissionsCollection = new PermissionsCollection()
  if (!await permissionsCollection.userHasPermission(username)) {
    console.log('Access violation attempt: the user ' + username + ' tried to use the bot. Text from the user: ' + ctx.message?.text)
    void ctx.reply('Non sei autorizzato a utilizzare questo bot. Per favore, chiedi l\'accesso a @yatagarasu.\n\nYou are not allowed to use this bot. Please ask @yatagarasu for access.\n\nこのボットの使用は許可されていません。アクセス権を取得するには、@yatagarasu にお問い合わせください。')
    return
  }
  await next()
}

export function onlyParents (ctx: Context): void {
  const username = ctx.from?.username
  if (username == null) {
    throw new Error('username is null')
  }
  if (username !== 'yatagarasu' && username !== 'Ai_Kibo') {
    console.log('Access violation attempt: the user ' + username + ' tried to use the bot.')
    void ctx.reply('Non sei autorizzato a utilizzare questo bot. Per favore, chiedi l\'accesso a @yatagarasu.\n\nYou are not allowed to use this bot. Please ask @yatagarasu for access.\n\nこのボットの使用は許可されていません。アクセス権を取得するには、@yatagarasu にお問い合わせください。')
    throw new Error('Access violation attempt: the user ' + username + ' tried to use an admin-only command.')
  }
}
