import { type Bot } from 'grammy'
import { addPermissionCommand } from '../commands/addPermission'
import { removePermissionCommand } from '../commands/removePermission'
import { menu, promtpMessage } from '../menu/mainMenu'
import { addComeStaiMessage } from '../commands/addComeStaiMessage'
import { resetComeStaiMessage } from '../commands/resetComeStaiMessage'
import { showHelp } from '../commands/showHelp'
import { showLogs } from '../commands/showLogs'
import { showLogsByUser } from '../commands/showLogsByUser'

export function addCommands (bot: Bot): void {
  bot.command('help', showHelp)
  bot.command('logs', showLogs)
  bot.command('logsbyuser', showLogsByUser)
  bot.command('addcomestai', addComeStaiMessage)
  bot.command('resetcomestai', resetComeStaiMessage)
  bot.command('addPermission', addPermissionCommand)
  bot.command('removePermission', removePermissionCommand)
  bot.command('start', (ctx, next) => {
    void ctx.reply(promtpMessage, { reply_markup: menu }).then(next)
  })
  bot.command('menu', (ctx, next) => {
    void ctx.reply(promtpMessage, { reply_markup: menu }).then(next)
  })
}
