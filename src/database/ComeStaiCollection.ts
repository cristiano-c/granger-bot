import { type Collection, type Db, type WithId } from 'mongodb'
import MongoDriver from './MongoDriver'

const comeStaiCollection = 'ComeStai'

export interface Message {
  message: string
}

export class ComeStaiCollection {
  private static instance: ComeStaiCollection
  private readonly db: Db
  private readonly collection: Collection<Message>
  private circularIndex: number = 0

  private constructor () {
    this.db = MongoDriver.getInstance().getDb()
    this.collection = this.db.collection(comeStaiCollection)
  }

  static getInstance (): ComeStaiCollection {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!ComeStaiCollection.instance) {
      ComeStaiCollection.instance = new ComeStaiCollection()
    }
    return ComeStaiCollection.instance
  }

  getCircular (range: number): number {
    const ts = Math.round((new Date()).getTime() / 1000)
    this.circularIndex = this.circularIndex + Math.floor(ts)
    const i = this.circularIndex % range
    this.circularIndex = this.circularIndex + 1
    return i
  }

  async getRandomMessage (): Promise<string> {
    const messages: Array<WithId<Message>> = await this.collection.find().toArray()
    const ciurcularIndex: number = this.getCircular(messages.length)
    return messages[ciurcularIndex]?.message ?? 'Oggi sto benone, cresco sempre di più... grazie per avermelo chiesto! ❤️'
  }

  async insertOne (message: string): Promise<void> {
    await this.collection.insertOne({ message })
  }

  async deleteMany (): Promise<void> {
    await this.collection.deleteMany({})
  }
}
