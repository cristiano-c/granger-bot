import { type Db, type Collection, type IndexSpecification } from 'mongodb'
import MongoDriver from '../database/MongoDriver'

const permissionsCollectionName = 'permissions'

interface PermissionDocument {
  username: string
  uploaded: Date
}

class PermissionsCollection {
  private db: Db
  private readonly collectionName: string

  constructor () {
    this.collectionName = permissionsCollectionName
    this.db = MongoDriver.getInstance().getDb()
    void this.createUniqueUsernameIndex(this.db.collection(this.collectionName))
  }

  private getDb (): Db {
    if (this.db === null) {
      this.db = MongoDriver.getInstance().getDb()
      return this.db
    } else {
      return this.db
    }
  }

  async userHasPermission (username: string): Promise<boolean> {
    try {
      this.getDb()
      const collection = this.db.collection(this.collectionName)
      const result = await collection.findOne({ username })
      return result !== null
    } catch (err) {
      console.error('Error getting permission:', err)
      throw err
    }
  }

  async addUserPermission (username: string): Promise<void> {
    try {
      this.getDb()
      const permissionDocument: PermissionDocument = {
        username,
        uploaded: new Date()
      }

      const collection = this.db.collection(this.collectionName)
      await collection.insertOne(permissionDocument)
    } catch (err) {
      console.error('Error saving permission:', err)
      throw err
    }
  }

  async removeUserPermission (username: string): Promise<void> {
    try {
      this.getDb()
      const collection = this.db.collection(this.collectionName)
      await collection.deleteOne({ username })
    } catch (err) {
      console.error('Error removing permission:', err)
      throw err
    }
  }

  async createUniqueUsernameIndex (collection: Collection): Promise<void> {
    try {
      // Create a unique index on the "username" field
      if (await this.indexExistsInCollection(collection, 'uniqueUsername')) {
        await collection.createIndex({ username: 1 }, { name: 'uniqueUsername', unique: true })
      }
    } catch (error) {
      console.error('Error creating the unique index:', error)
      throw error
    }
  }

  async indexExistsInCollection (collection: Collection, indexName: string): Promise<boolean> {
    const indexes = await collection.indexes()

    for (const index of indexes) {
      if (index.name === indexName) {
        return true
      }
    }

    return false
  }
}

export default PermissionsCollection
