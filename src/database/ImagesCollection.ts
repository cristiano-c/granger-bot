import { type Db } from 'mongodb'
import axios from 'axios'
import MongoDriver from '../database/MongoDriver'

const imagesCollectionName = 'images'

let indexCircularOffset = 0

export interface ImageDocument {
  data: string // Base64
  name: string
  uploaded: Date
  description?: string
}

class ImagesCollection {
  private db: Db
  private readonly collectionName: string

  constructor () {
    this.collectionName = imagesCollectionName
    this.db = MongoDriver.getInstance().getDb()
  }

  private getDb (): Db {
    if (this.db === null) {
      this.db = MongoDriver.getInstance().getDb()
      return this.db
    } else {
      return this.db
    }
  }

  async getImageBase64ByName (name: string): Promise<string | null> {
    try {
      this.getDb()
      const collection = this.db.collection(this.collectionName)
      const imageDocument = await collection.findOne({ name })
      return imageDocument?.data
    } catch (err) {
      console.error('Error getting image:', err)
      throw err
    }
  }

  async getRandomImageOfCurrentDay (): Promise<ImageDocument> {
    try {
      this.getDb()
      const collection = this.db.collection(this.collectionName)
      const imageDocuments = await collection.aggregate([
        {
          $match: {
            uploaded: {
              $gte: new Date(new Date().setHours(0, 0, 0, 0)),
              $lt: new Date(new Date().setHours(23, 59, 59, 999))
            }
          }
        }
      ]).toArray()
      const index = indexCircularOffset % imageDocuments.length
      indexCircularOffset = indexCircularOffset + 1
      return imageDocuments[index] as ImageDocument
    } catch (err) {
      console.error('Error getting image:', err)
      throw err
    }
  }

  async getRandomImage (): Promise<ImageDocument> {
    try {
      this.getDb()
      const collection = this.db.collection(this.collectionName)
      const imageDocuments = await collection.find().toArray()
      const ts = Math.round((new Date()).getTime() / 1000)
      indexCircularOffset = indexCircularOffset + Math.floor(ts)
      const index = indexCircularOffset % imageDocuments.length
      indexCircularOffset = indexCircularOffset + 1
      return imageDocuments[index] as unknown as ImageDocument
    } catch (err) {
      console.error('Error getting image:', err)
      throw err
    }
  }

  async saveImage (data: any, fileName: string, description?: string): Promise<void> {
    try {
      this.getDb()
      const imageDocument: ImageDocument = {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        data: Buffer.from(data).toString('base64'),
        name: fileName,
        uploaded: new Date(),
        description: description ?? ''
      }

      const collection = this.db.collection(this.collectionName)
      await collection.insertOne(imageDocument)
    } catch (err) {
      console.error('Error saving image:', err)
      throw err
    }
  }

  async sendImageToExternalService (name: string, externalServiceUrl: string): Promise<void> {
    try {
      this.getDb()
      const base64Data = await this.getImageBase64ByName(name)
      if (base64Data == null) {
        throw new Error('Image not found')
      }

      await axios.post(externalServiceUrl, {
        data: base64Data,
        name
      })
    } catch (err) {
      console.error('Error sending image to external service:', err)
      throw err
    }
  }
}

export default ImagesCollection
