import { MongoClient, type Db } from 'mongodb'

class MongoDriver {
  private static instance: MongoDriver | null = null
  private db: Db | null = null

  private constructor () {}

  public static getInstance (): MongoDriver {
    if (this.instance == null) {
      this.instance = new MongoDriver()
    }
    return this.instance
  }

  public async initialize (dbUrl: string, dbName: string): Promise<void> {
    try {
      console.log('Connecting to MongoDB...')
      const client = await MongoClient.connect(dbUrl)
      this.db = client.db(dbName)
      console.log('Connected to MongoDB')
    } catch (err) {
      console.error('Error connecting to MongoDB:', err)
      throw err
    }
  }

  public getDb (): Db {
    if (this.db == null) {
      throw new Error('MongoDB has not been initialized. Call initialize() first.')
    }
    return this.db
  }
}

export default MongoDriver
