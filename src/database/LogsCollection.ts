import { type Collection, type Db } from 'mongodb'
import MongoDriver from './MongoDriver'

const logsCollectionName = 'logs'

export interface Log {
  username: string
  tracked: Date
  action: string
  message: string | undefined
}

export class LogsCollection {
  private static instance: LogsCollection
  private db: Db
  private readonly collection: Collection<Log>

  private getDb (): Db {
    if (this.db === null) {
      this.db = MongoDriver.getInstance().getDb()
      return this.db
    } else {
      return this.db
    }
  }

  private constructor () {
    this.db = MongoDriver.getInstance().getDb()
    this.collection = this.db.collection(logsCollectionName)
  }

  static getInstance (): LogsCollection {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!LogsCollection.instance) {
      LogsCollection.instance = new LogsCollection()
    }
    return LogsCollection.instance
  }

  async find (query: object): Promise<Log[]> {
    return await this.collection.find(query).toArray()
  }

  async getLastNDocuments (n: number): Promise<Log[]> {
    return await this.collection.find().sort({ tracked: -1 }).limit(n).toArray()
  }

  async getLastNDocumentsByUser (username: string, n: number): Promise<Log[]> {
    return await this.collection.find({ username }).sort({ tracked: -1 }).limit(n).toArray()
  }

  async findOne (query: object): Promise<Log | null> {
    return await this.collection.findOne(query)
  }

  async findByUsername (username: string): Promise<Log[]> {
    return await this.find({ username })
  }

  async insertOne (log: Log): Promise<void> {
    await this.collection.insertOne(log)
  }
}
