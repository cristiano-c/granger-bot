import { run } from '@grammyjs/runner'
import { addListeners } from './configurations/listeners'
import { addMiddlewares } from './configurations/middlewares'
import { addMenus } from './configurations/menus'
import { addCommands } from './configurations/commands'
import { startupConfiguration } from './configurations/startup'

async function main (): Promise<void> {
  const bot = await startupConfiguration()

  addMenus(bot)
  addCommands(bot)
  addMiddlewares(bot)
  addListeners(bot)

  const handle = run(bot)

  if (handle.isRunning()) {
    console.log('Bot is running!')
  } else {
    console.log('Bot is not running')
  }
}

main().then(r => {
  console.log(r)
}).catch(err => {
  console.error(err)
})
