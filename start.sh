#!/bin/bash

# Command to run
COMMAND="npx ts-node src/main"

# Run the command in the background with nohup and &
nohup $COMMAND > /dev/null 2>&1 &

# Save the PID of the background process
echo $! > .pidfile

echo "Started the command '$COMMAND' in the background with PID $!"
